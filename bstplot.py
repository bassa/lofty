#!/usr/bin/env python3
import os
import glob
import argparse

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter, MultipleLocator

from lofty.io import read_bst, read_minio_bst
from lofty.plotting import plot_bst_timeseries, plot_bst_dynamic_spectrum

import astropy.units as u
from astropy.time import Time

if __name__ == "__main__":
    # Read command line arguments
    parser = argparse.ArgumentParser(description="Plot BST data")
    parser.add_argument("-m", "--minio", help="Minio HDF5", action="store_true")
    parser.add_argument("-d", "--dynspec", help="Plot dynamic spectrum", action="store_true")
    parser.add_argument("-t", "--timeseries", help="Plot timeseries", action="store_true")
    parser.add_argument("-s", "--subband", help="Subband to plot timeseries [int, default: 300]", default=300, type=int)
    parser.add_argument("-p", "--pol", help="Polarization to plot dynamic spectrum [stokes/X/Y, default: stokes]", default="stokes")
    parser.add_argument("filenames", help="SST filenames", nargs="*", metavar="FILE")
    args = parser.parse_args()

    # Read data
    if args.minio:
        metadata, data = read_minio_bst(args.filenames)
    else:
        metadata, data = read_bst(args.filenames)

    print(data.shape)
    print(metadata)
    t = Time("2024-10-10T09:00:00", format="isot", scale="utc") + np.arange(86305) * u.s
    metadata["timestamps"] = t.isot
    
    plot_bst_dynamic_spectrum(metadata, data, args.pol)
    
    plot_bst_timeseries(metadata, data, args.subband)
