#!/usr/bin/env python3
import os
import glob
import argparse

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter, MultipleLocator

from lofty.io import read_xst, read_minio_xst
from lofty.plotting import plot_xst_matrix

if __name__ == "__main__":
    # Read command line arguments
    parser = argparse.ArgumentParser(description="Plot XST data")
    parser.add_argument("-m", "--minio", help="Minio HDF5", action="store_true")
    parser.add_argument("filenames", help="XST filenames", nargs="*", metavar="FILE")
    args = parser.parse_args()

    print(args.filenames)
    
    # Read data
    metadata, data = read_xst(sorted(args.filenames))

    for key in metadata.keys():
        print(key, metadata[key])

    print(metadata["subbands"])
    print(metadata["nsub"])
    
    for isub in range(metadata["nsub"]):
        plot_xst_matrix(metadata, data, isub)
